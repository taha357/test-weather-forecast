const express = require('express');
const https = require('https');
const rp = require('request-promise');
const cors = require('cors');

const app = express();
app.use(cors());

app.get('/api/getWeather', async (req, res) => {
  const data = await rp('https://api.openweathermap.org/data/2.5/forecast?q=Karachi,pk&units=metric&appid=3bc8a79afc9bcd1aa521e375b3bd8b00');
  res.send(data);
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log('Server started at Port: ' + PORT)
});
