import React, { Component } from 'react';
import Card from './Card';

class Forecast extends Component {
  constructor(props){
    super(props);
    this.state = { days: [] };
  }

  componentWillMount() {
    fetch('http://localhost:5000/api/getWeather')
      .then(function(response){
        return response.json();
      }).then(function(response){
        const days = {};
        const dayNames = ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'];

        for (let list of response.list) {
          const obj = new Date(list.dt_txt);
          const date = obj.getDate();
          days[date] = days[date] || {};
          days[date].day = dayNames[obj.getDay()];
          days[date].count = days[date].count || 0;
          days[date].temp_max = days[date].temp_max || 0;
          days[date].temp_min = days[date].temp_min || 0;
          days[date].temp_max += list.main.temp_max;
          days[date].temp_min += list.main.temp_min;
          days[date].count++;
          days[date].icon = list.weather[0].icon;
          days[date].forecast = days[date].forecast || [];
          days[date].forecast[list.dt_txt] = list.main.temp;
        }
        this.setState({ days });
      }.bind(this));
  }

  renderCards() {
    // console.log(this.state.days)
    if(this.state.days.length === 0) { return }
    const days = this.state.days;
    const object = Object.keys(days).map(key =>
        <Card key={key}>{days[key]}</Card>
    );
    return object;
  }

  render() {
    return (
        <div style={{display: 'inline-block'}}>
          {this.renderCards()}
        </div>
    );
  }
}

export default Forecast;
