import React, { Component } from 'react';
import ReactTooltip from 'react-tooltip'


class Card extends Component {
  render() {
    const day = this.props.children;
    return (
        <span style={card} data-tip data-for='temperature'>

          <span>
            {day.day}
          </span>
          <span>
            <img src={`http://openweathermap.org/img/w/${day.icon}.png`}/>
          </span>
          <span>
            {`${parseInt(day.temp_max/day.count)}\u00b0C`} - {`${parseInt(day.temp_min/day.count)}\u00b0C`}
          </span>


          <ReactTooltip id='temperature' aria-haspopup='true' role='example'>
           <ul>
             {this.renderTooltip()}
           </ul>
          </ReactTooltip>
        </span>
    );
  }

  renderTooltip() {
    const forecast = this.props.children.forecast;
    const object = Object.keys(forecast).map(key =>
      <li key={key}>{key} - {forecast[key]+'\u00b0C'}</li>
    );
    return object;
  }
}

const card = {
  border: '1px solid black',
  height: '100px',
  width: '100px',
  margin: '20px',
  display: 'inline-block'
};

export default Card;
