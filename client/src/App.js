import React, { Component } from 'react';
import './App.css';
import Forecast from './components/Forecast';

class App extends Component {
  render() {
    return (
        <Forecast/>
    );
  }
}

export default App;
